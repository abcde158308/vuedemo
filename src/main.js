import Vue from 'vue'
//import ElementUI from 'element-ui'
//import 'element-ui/lib/theme-default/index.css'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import  './css/iconfont.css'
import App from './App.vue'
import VueRouter from "vue-router";
import VueResource from 'vue-resource'
import appconfig from './js/app.js'
import $ from "jquery"
import md5 from 'blueimp-md5'
//开启debug模式
Vue.config.debug = true;
//Vue.use(ElementUI);
Vue.use(MintUI);
Vue.use(VueRouter);
Vue.use(VueResource);
window.appconfig=appconfig;
window.$=$;
window.md5=md5;
appconfig.ip='https://dev.thb100.com/';

// 定义组件, 也可以像教程之前教的方法从别的文件引入
const First = { template: '<div><h2>我是第 1 个子页面</h2></div>' }
import secondcomponent from './component/secondcomponent.vue'
import  home from  './component/home.vue'
import  login from  './component/login.vue'
import  details from './component/productdetails.vue'


// 创建一个路由器实例
// 并且配置路由规则
const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      component: login
    },
    {
      path: '/second/:id/:username',
      component: secondcomponent
    },
    {
      path: '/home',
      component: home
    },
    {
      path: '/details',
      component: details
    }

  ]
})

// 现在我们可以启动应用了！
// 路由器会创建一个 App 实例，并且挂载到选择符 #app 匹配的元素上。
const app = new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')